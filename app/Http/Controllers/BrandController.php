<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use DB;


class BrandController extends Controller
{
    public function index(){
        return view('admin.brand.add-brand');
    }

    public function saveBrandInfo(Request $request){
        /*
        $result= $request->all();
    	echo "<pre>";
        print_r($result);
        */
        $this->validate($request, [
            'brand_name'    =>'required|regex:/^[\pL\s\-]+$/u|max:10|min:4',
            'brand_dis'     =>'required',
            'brand_status'  =>'required',
        ]);

        $brand =new Brand();
       
        $brand->brand_name =$request->brand_name;
        $brand->brand_dis  =$request->brand_dis;
        $brand->brand_status=$request->brand_status;
        $brand->save();
        
        return redirect('/brand/add')->with('message', 'Brand info save successfully');
    }
    public function  manageBrandInfo(){
        $brands =Brand::paginate(10);

        return view('admin.brand.manage-brand',['brands'=>$brands]);
    }
    public function publishedBrandInfo($id){
        $brand =Brand::find($id);
        $brand->brand_status =1;
        $brand->save();
        return redirect('/brand/manage')->with('message','Brand info Published');
    }

    public function unpublishedBrandInfo($id){
        $brand =Brand::find($id);
        $brand->brand_status =0;
        $brand->save();
        return redirect('/brand/manage')->with('message','Brand info unPublished');
    }
    public function editBrandInfo($id){
        $brand =Brand::find($id);
        return view('admin.brand.edit-brand',['brand' =>$brand]);
    }
    public function updateBrandInfo(Request $request){
        //return $request->all();
        $this->validate($request,[
            'brand_name'    =>'required|regex:/^[\pL\s\-]+$/u|max:10|min:4',
            'brand_dis'     =>'required',
            'brand_status'  =>'required',
        ]);
        $brand = Brand::find($request->id);

        $brand->brand_name =$request->brand_name;
        $brand->brand_dis  =$request->brand_dis;
        $brand->brand_status =$request->brand_status;
        $brand->save();

        return redirect('/brand/manage')->with('message','Brand Info update Successfully');
    }

    public function deleteBrandInfo($id){
        $brand =Brand::find($id);
        $brand->delete();

        return redirect('/brand/manage')->with('message','Category info delete Successfully');
    }

}
