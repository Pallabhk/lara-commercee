<?php

namespace App\Http\Controllers;
use App\Category;
use App\Brand;
use App\Product;
use Image;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
  public function index(){ 
        $brands     = Brand::Where('brand_status', 1)->get();
        $categories = Category::Where('publication_status', 1)->get();
      
       return view('admin.product.add-product',[
        'brands'     => $brands,
        'categories' => $categories
           
       ]);
   }
  

   public function saveProductInfo(Request $request){
    //Validation 
    $this->validate($request,[
      'product_name'   =>'required',
      'product_code'   =>'required',
      'product_color'  =>'required',
      'description'    =>'required',
      'price'          =>'required',
      'image'          =>'required',
      'product_status'  =>'required',
      
    ]);

    $product  =new Product();
    $product->brand_id       =$request->brand_id;
    $product->category_id    =$request->category_id;
    $product->product_name   =$request->product_name;
    $product->product_code   =$request->product_code;
    $product->product_color  =$request->product_color;
    $product->description    =$request->description;
    $product->price          =$request->price;

    if($request->hasfile('image')){
      $image_tmp =Input::file('image');
      if($image_tmp->isValid()){
        $extension =$image_tmp->getClientOriginalExtension();
        
        $filename         =rand(111,9999).'.'.$extension;
        $large_image_path ='products/large/'.$filename;
        $medium_image_path='products/medium/'.$filename;
        $small_image_path ='products/small/'.$filename;

        //Resize Image
        Image::make($image_tmp)->save($large_image_path);
        Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
        Image::make($image_tmp)->resize(300,300)->save($small_image_path);
        $product->image =$filename;

      }
    }
    $product->product_status =$request->product_status;
    $product->save();
     return redirect('/product/add')->with('message','Product info save successfully');
      
   }
   //Manage product name
    public function manageProductInfo(){
      $products =DB::table('products')
                ->join('brands','products.brand_id', '=','brands.id')
                ->join('categories','products.category_id', '=' ,'categories.id')
                ->select('products.*','brands.brand_name','categories.category_name')
                ->get();
              // return $products;

      return view('admin.product.manage-product',['products' =>$products]);
    }

    public function editProductInfo($id){
      $category   =Category::Where('publication_status',1)->get();
      $brand      =Brand::Where('brand_status',1)->get();
      $product    =Product::find($id);

      return view('admin.product.edit-product',[
            'product' =>$product,
            'brand'   =>$brand,
            'category'=>$category,
      ]);

  }
  public function updateProductInfo( Request $request,'POST'){
    
    
    $product =Product::find($request->product_id);
    $product->brand_id       =$request->brand_id;
    $product->category_id    =$request->category_id;
    
    $product->product_name   =$request->product_name;
    $product->product_code   =$request->product_code;
    $product->product_color  =$request->product_color;
    $product->description    =$request->description;
    $product->price          =$request->price;

    if($request->hasfile('image')){
      $image_tmp =Input::file('image');
      if($image_tmp->isValid()){
        $extension =$image_tmp->getClientOriginalExtension();
        
        $filename         =rand(111,9999).'.'.$extension;
        $large_image_path ='products/large/'.$filename;
        $medium_image_path='products/medium/'.$filename;
        $small_image_path ='products/small/'.$filename;

        //Resize Image
        Image::make($image_tmp)->save($large_image_path);
        Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
        Image::make($image_tmp)->resize(300,300)->save($small_image_path);
        $product->image =$filename;

      }
    }
    $product->product_status =$request->product_status;
    $product->save();
    return redirect('/product/manage')->with('message', 'Product info update Successfully');
  }

//Add Attribute
  public function addAttribute(Request $request,$id=null){
    $product =Product::Where(['id'=>$id])->first();
   
      $attribute =$request->all();
      print_r('$attribute');
      echo "</pre>";
 
    
    return view('admin.product.add-attribute',['product' =>$product]);
  }
}
