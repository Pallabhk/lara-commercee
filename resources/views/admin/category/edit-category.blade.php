@extends('admin.master')
@section('body')
<div id="content">
<div class="container-fluid"><hr>
	<div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Edit Category</h5>
          </div>
			 <h3 class="text-center text-success"></h3>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ route('update-category')}}" name="add_category" id="add_category" novalidate="novalidate">
              {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Category Name</label>
                <div class="controls"> 
                  <input type="text" name="category_name" value="{{ $category->category_name }}" id="category_name"></br>
                  <input type="hidden" name="id" value="{{ $category->id}}" id="id"></br>
                  <span class="text-error">{{ $errors->has('category_name') ? $errors->first('category_name') : ' ' }}</span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls">
                 <textarea name="description" id="description" >{{ $category->description}}</textarea></br>
                 <span class="text-error">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span> 
                </div>
              </div>
              <div class="control-group">
              <label class="control-label">Status</label>
               <div class="controls">
                <label><input type="radio"  name="publication_status" {{ $category->publication_status == 1 ? 'checked' : '' }} value="1"/> Published</label>
                <label><input type="radio"  name="publication_status" {{ $category->publication_status == 0 ? 'checked' : '' }} value="0"/> UnPublished</label>
                <span class="text-error">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span> 
              </div>
            </div>
         
               <div class="form-actions">
                <input type="submit" value="Update Category" class="btn btn-success">
              </div>
            
      </div>
      
      </form>
          </div>
        </div>
      </div>
    </div>
</div>
 </div> 

@endsection