@extends('admin.master')
@section('body')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Category Tables</a> </div>
    <h1>Category Tables</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Category table</h5>
            <h4 class="text-center text-success">{{ Session::get('message')}}</h4>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Category Id</th>
                  <th>Category</th>
                  <th>Description</th>
                  <th>Publication Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @php($i=1)
                @foreach($categories as $category)
                <tr class="gradeX">
                  <td>{{ $i++ }}</td>
                  <td>{{ $category->category_name}}</td>
                  <td>{{ $category->description}}</td>
                  <td>{{ $category->publication_status ==1 ? 'Published' : 'Unpublished' }}</td>
                  <td>
                   @if($category->publication_status ==1)
                <a href="{{ route('unpublished-category', ['id'=>$category->id])}}" class="btn btn-info btn-xs">
                  <i class="icon-circle-arrow-up"></i>
                </a>
                @else
                <a href="{{ route('published-category', ['id'=>$category->id])}}" class="btn btn-warning btn-xs">
                  <i class="icon-circle-arrow-down"></i>
                </a>
                @endif
                <a href="{{ route('edit-category',['id'=>$category->id])}}" class="btn btn-primary btn-xs">
                  <i class="icon-edit"></i>
                </a>
                <a href="{{ route('delete-category',['id'=>$category->id])}}" class="btn btn-danger btn-xs">
                  <i class="icon-trash"></i>
                </a>
                  </td>
                </tr>
               
               @endforeach
              </tbody>
            </table>
          </div>
          <div class="text-right">
            <ul class="pagination">
              <li class="active">
              {{ $categories->links() }}
              </li>  
            </ul>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>

@endsection